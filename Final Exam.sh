%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

1. Bash 

$i = ith entry
$* = All entries

echo "output message"

#!/bin/bash          
STR="Hello World!"
echo $STR

#!/bin/bash          
OF=/var/my-backup-$(date +%Y%m%d).tgz
tar -cZf $OF /home/me/

#!/bin/bash
if [ "foo" = "foo" ]; then
echo expression evaluated as true
else
echo expression evaluated as false
fi


#!/bin/bash
        for i in `seq 1 10`;
        do
                echo $i
        done    

#!/bin/bash 
         COUNTER=0
         while [  $COUNTER -lt 10 ]; do
             echo The counter is $COUNTER
             let COUNTER=COUNTER+1 
         done

#!/bin/bash 
           function quit {
               exit
           }
           function hello {
               echo Hello!
           }
           hello
           quit
           echo foo                  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bibtex source
makeindex source
pdflatex source.tex

sqlite3 database.db
.read file.sql
.databases
.tables
Select * from tables