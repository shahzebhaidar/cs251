x=0:50;

y = load("y");


figure(1);
 hist(load("h1.hist"),x);
xlabel('x_{Values}');
ylabel('Frequency');
title('Histogram - 1, \mu = 15,\sigma = \sqrt{10.5}');

figure(2); 
hist(load("h2.hist"),x);
xlabel('x_{Values}');
ylabel('Frequency')
title('Histogram - 2');

figure(3); 
hist(load("h3.hist"),x);
xlabel('x_{Values}');
ylabel('Frequency');
title('Histogram - 3');


figure(4); 
hist(load("h4.hist"),x);
xlabel('x_{Values}');
ylabel('Frequency');
title('Histogram - 4');

figure(5); 
hist(load("h5.hist"),x);
xlabel('x_{Values}');
ylabel('Frequency');
title('Histogram - 5');

figure(6); 
hist(load("h6.hist"),x);
xlabel('x_{Values}');
ylabel('Frequency');
title('Histogram - 6');

figure(7);
plot(x,y,'k-o');