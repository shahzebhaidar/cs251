% Script to calculate and plot a rectified sine wave
%count=0;
format short
A=[];
n=10;
while(n<=1000000)

count=0; 

for p=1:n

x = 2*rand;
y = 2*rand;
if (x-1)^2 + (y-1)^2 <= 1 
   count = count+1;
end
 
end

n;
  piapprox = 4*count/n; 
 %delta = pi - piapprox;

 A = [ A;  n,piapprox ]  ;
n = n*10;
end
disp("    Numer     esfs");
disp(A);
save file1 A