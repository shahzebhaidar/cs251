% Script to calculate and plot a rectified sine wave
%count=0;
B = [1,2,3,4,5,6];
C = [pi,pi,pi,pi,pi,pi];
x = logspace(10,1000000,100);
format short
A=[];
n=10;
while(n<=1000000)

count=0; 

for p=1:n

x = 2*rand;
y = 2*rand;
if (x-1)^2 + (y-1)^2 <= 1 
   count = count+1;
end
 
end

n;
  piapprox = 4*count/n; 
 %delta = pi - piapprox;

 A = [ A,piapprox ]  ;
n = n*10;
end
plot(B,C,'-ro','linewidth',2,B,A,'k-+','linewidth',2);
title('Estimates of Pi');
ylabel('Estimate');
xlabel('Number Of Iterations (log10scale)');
%print -deps foo.eps
 h = legend ({'Exact pi'}, 'Approximate values');
 legend (h, 'location', 'northeastoutside');
 set (h, 'fontsize', 12);
 %hold on;
 print -deps image.eps