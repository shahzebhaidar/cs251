x = 0:50;


y = exp(- ((x-15).^2)/(2* 10.5))/ sqrt(2*pi*10.5);
file=fopen("y","w");
for i=1:51
	fprintf(file,"%f\n",y(i));
end
fclose(file);



s1=normrnd(15,sqrt(10.5),[10,1]);
h1=hist(s1,x);
file=fopen("h1.hist","w");
for i=1:10
	fprintf(file,"%f\n",s1(i));
end
fclose(file);



s2=normrnd(15,sqrt(10.5),[100,1]);
h2=hist(s2,x);
file=fopen("h2.hist","w");
for i=1:100
	fprintf(file,"%f\n",s2(i));
end
fclose(file);



s3=normrnd(15,sqrt(10.5),[1000,1]);
h3=hist(s3,x);
file=fopen("h3.hist","w");
for i=1:1000
	fprintf(file,"%f\n",s3(i));
end
fclose(file);



s4=normrnd(15,sqrt(10.5),[10000,1]);
h4=hist(s4,x);
file=fopen("h4.hist","w");
for i=1:10000
	fprintf(file,"%f\n",s4(i));
end
fclose(file);



s5=normrnd(15,sqrt(10.5),[100000,1]);
h5=hist(s5,x);
file=fopen("h5.hist","w");
for i=1:100000
	fprintf(file,"%f\n",s5(i));
end
fclose(file);



s6=normrnd(15,sqrt(10.5),[1000000,1]);
h6=hist(s6,x);
file=fopen("h6.hist","w");
for i=1:1000000
	fprintf(file,"%f\n",s6(i));
end
fclose(file);