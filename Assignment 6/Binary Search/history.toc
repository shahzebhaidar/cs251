\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Algorithm and Implementation}{2}
\contentsline {subsection}{\numberline {2.1}Building upon the idea}{2}
\contentsline {subsection}{\numberline {2.2}Implementation}{3}
\contentsline {section}{\numberline {3}Binary Search vs Linear Search}{3}
\contentsline {subsection}{\numberline {3.1}Time Complexity}{3}
\contentsline {subsubsection}{\numberline {3.1.1}Linear Search}{3}
\contentsline {subsubsection}{\numberline {3.1.2}Binary Search}{3}
\contentsline {subsubsection}{\numberline {3.1.3}Comparison}{3}
\contentsline {subsection}{\numberline {3.2}Space Complexity}{3}
\contentsline {subsection}{\numberline {3.3}Camparison Tables}{4}
\contentsline {section}{\numberline {4}Conclusion}{4}
