\documentclass{article}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{verbatim}
\usepackage{booktabs}
\usepackage[a4paper,bindingoffset=0.2in,%
            left=1in,right=1in,top=1in,bottom=1in,%
            footskip=.25in]{geometry}

\makeindex
\title{Indian Revolt of 1857}
\author{Shahzeb Haidar}
\date{28 Feb 2016}

\begin{document}

\maketitle
\bibliographystyle{plain}
\tableofcontents
\listoffigures
%\listoftables


\newpage
\begin{abstract}
\begin{flushleft}

The Indian\index{Indian} Rebellion\index{Rebellion} of 1857\index{1857} refers to a rebellion in India against the rule of the British\index{British} East India Company\index{East India Company}, that ran from May 1857 to June 1858. The rebellion began as a mutiny of sepoys\index{sepoys} of the East India Company's army on 10 May 1857, in the cantonment of the town of Meerut\index{Meerut}, and soon escalated into other mutinies and civilian rebellions largely in the upper Gangetic plain and central India, with the major hostilities confined to present-day Uttar Pradesh, Bihar, northern Madhya Pradesh, and the Delhi\index{Delhi} region. The rebellion posed a considerable threat to East India Company power in that region, and was contained only with the fall of Gwalior on 20 June 1858. The rebellion has been known by many names, including India's First War of Independence, the Great Rebellion, the Indian Rebellion, the Indian Mutiny, the Revolt of 1857, the Rebellion of 1857, the Uprising of 1857, the Sepoy Rebellion, the Indian Insurrection and the Sepoy Mutiny.


\end{flushleft}
\end{abstract}


\section{Introduction}
 
  British East India Company had established a presence in India as far back as 1612, and earlier administered the factory areas established for trading purposes, its victory in the Battle of Plassey\index{Plassey} in 1757 marked the beginning of its firm foothold in Eastern India. The victory was consolidated in 1764 at the Battle of Buxar, when the East India Company army defeated Mughal\index{Mughal} emperor, Shah Alam II. After his defeat, the Mughal Emperor granted the Company the right for collection of Revenue in the provinces of Bengal, Bihar, and Odisha known as $Diwani$ to the Company. The Company soon expanded its territories around its bases in Bombay and Madras and, later, the Anglo-Mysore Wars (1766$-$1799) and the Anglo-Maratha Wars (1772-1818) led to control of the vast regions of India.~\cite{BOOK1}.

  \vspace{1em}
  
  
  \begin{figure}[!h]
  \begin{center}
  \includegraphics[width=70mm,scale=0.65]{India1837to1857.jpg}
  \caption{Area in pink denotes the Company territory in 1837(left) and 1857(right)}
  \label{Sorted Array}
  \end{center}
  \end{figure}
  
  Accelerated expansion of Company territories was achieved either by subsidiary alliances between the Company and local rulers or by direct military annexation. The subsidiary alliances created the princely states\index{Princely states} of the Hindu maharajas and the Muslim nawabs. Punjab, North-West Frontier Province, and Kashmir were annexed after the Second Anglo-Sikh War\index{Anglo-Sikh War} in 1849; however, Kashmir was immediately sold under the Treaty of Amritsar (1846) to the Dogra Dynasty of Jammu and thereby became a princely state.~\cite{BOOK2}. The border dispute between Nepal and British India, which sharpened after 1801, had caused the Anglo-Nepalese War of 1814–16 and brought the defeated Gurkhas under British influence. In 1854, Berar was annexed, and the state of Oudh was added two years later. For practical purposes, the Company was the government of much of India.
 



\section{Causes of the Rebellion}
The Indian Rebellion of 1857 occurred as the result of an accumulation of factors over time, rather than any single event.

The Bengal Army\index{Bengal Army} recruited higher castes, such as Rajputs and Bhumihar, mostly from the Awadh and Bihar regions, and even restricted the enlistment of lower castes in 1855. In contrast, the Madras Army and Bombay Army were 
more localized, caste-neutral armies that did not prefer high-caste men. The domination of higher castes in the Bengal Army has been blamed in part for initial mutinies that led to the rebellion.

\subsection{Tallow and lard-greased cartridges}
The final spark was provided by the ammunition for the new Enfield P-53 rifle. These rifles, which fired Minié balls, had a tighter fit than the earlier muskets, and used paper cartridges that came pre-greased. To load the rifle, sepoys had to bite the cartridge open to release the powder. The grease used on these cartridges was rumoured to include tallow derived from beef\index{beef}, which would be offensive to Hindus, and pork\index{pork}, which would be offensive to Muslims.~\cite{BOOK1}.

There had been rumours that the British sought to destroy the religions of the Indian people, and forcing the native soldiers to break their sacred code would have certainly added to this rumour, as it apparently did. The Company was quick to reverse the effects of this policy in hopes that the unrest would be quelled.

\subsection{Civilian disquiet}
The civilian rebellion was more multifarious. The rebels consisted of three groups: the feudal nobility, rural landlords called taluqdars, and the peasants. The nobility, many of whom had lost titles and domains under the Doctrine of Lapse, which refused to recognise the adopted children of princes as legal heirs, felt that the Company had interfered with a traditional system of inheritance. Rebel leaders such as Nana Sahib and the Rani of Jhansi belonged to this group; the latter, for example, was prepared to accept East India Company supremacy if her adopted son was recognised as her late husband's heir. 

The second group, the taluqdars\index{taluqdars}, had lost half their landed estates to peasant farmers as a result of the land reforms that came in the wake of annexation of Oudh. As the rebellion gained ground, the taluqdars quickly reoccupied the lands they had lost, and paradoxically, in part because of ties of kinship and feudal loyalty, did not experience significant opposition from the peasant farmers, many of whom joined the rebellion, to the great dismay of the British.

$Utilitarian\ and\ evangelical-inspired\ social\ reform$, including the abolition of sati and the legalisation of widow remarriage were considered by many—especially the British themselves—to have caused suspicion that Indian religious traditions were being $interfered\ with$, with the ultimate aim of conversion. Recent historians, including Chris Bayly, have preferred to frame this as a $clash of knowledges$, with proclamations from religious authorities before the revolt and testimony after it including on such issues as the $insults\ to\ women$, the rise of $low\ persons\ under\ British\ tutelage$, the $pollution$ caused by Western medicine and the persecuting and ignoring of traditional astrological authorities. European-run schools were also a problem: according to recorded testimonies, anger had spread because of stories that mathematics was replacing religious instruction, stories were chosen that would bring contempt upon Indian religions, and because girl children were exposed to $moral$ danger by education.


\subsection{The Bengal Army}
In the early years of Company rule, it tolerated and even encouraged the caste privileges and customs within the Bengal Army, which recruited its regular soldiers almost exclusively amongst the landowning Brahmins and Rajputs of the Bihar and Awadh.~\cite{BOOK3}. These soldiers were known as Purbiyas. By the time these customs and privileges came to be threatened by modernising regimes in Calcutta from the 1840s~\cite{BOOK3}. onwards, the sepoys had become accustomed to very high ritual status and were extremely sensitive to suggestions that their caste might be polluted.

The sepoys also gradually became dissatisfied with various other aspects of army life. Their pay was relatively low and after Awadh and the Punjab were annexed, the soldiers no longer received extra pay (batta or bhatta) for service there, because they were no longer considered $foreign\ missions$.~\cite{BOOK1}. The junior European officers became increasingly estranged from their soldiers, in many cases treated them as their racial inferiors. 

In 1856, a new Enlistment Act was introduced by the Company, which in theory made every unit in the Bengal Army liable to service overseas.~\cite{BOOK1}. Although it was intended to apply to only new recruits, the serving sepoys feared that the Act might be applied retroactively to them as well. A high-caste Hindu who travelled in the cramped, conditions of a wooden troop ship could not cook his own food on his own fire and accordingly risked losing caste through ritual pollution.



\section{The Revolt}
\begin{figure}[!h]
  \begin{center}
  \includegraphics[width=120mm,scale=1]{mutiny.jpg}
  %\includegraphics[width=70mm,scale=0.65]{luck.jpg}
  
  \caption{The major centers of mutiny}
  \label{Sorted Array}
  \end{center}
  \end{figure}

Bahadur Shah Zafar\index{Bahadur Shah Zafar} was proclaimed the Emperor of the whole of India.~\cite{BOOK2}. Most contemporary and modern accounts suggest that he was coerced by the sepoys and his courtiers to sign the proclamation against his will. In spite of the significant loss of power that the Mughal dynasty had suffered in the preceding centuries, their name still carried great prestige across northern India. The civilians, nobility and other dignitaries took the oath of allegiance to the emperor.

\begin{figure}[!h]
  %\begin{center}
  \includegraphics[width=70mm,scale=0.65]{delhi.jpg}
  \includegraphics[width=70mm,scale=0.65]{luck.jpg}
  
  \caption{Seige of Delhi(left) ; Lucknow in Ruins(right)}
  \label{Sorted Array}
  %\end{center}
  \end{figure}

\vspace{1em}
Although the rebels produced some natural leaders such as Bakht Khan, whom the Emperor later nominated as commander-in-chief after his son Mirza Mughal proved ineffectual, for the most part they were forced to look for leadership to rajahs and princes. Some of these were to prove dedicated leaders, but others were self-interested or inept.
\vspace{1em}

It took time to organise the European troops already in India into field forces, but eventually two columns left Meerut and Simla. They proceeded slowly towards Delhi and fought, killed, and hanged numerous Indians along the way.The Company established a base on the Delhi ridge to the north of the city and the Siege of Delhi began. The siege lasted roughly from 1 July to 21 September.~\cite{BOOK4}. 

\vspace{1em}
Very soon after the events in Meerut, rebellion erupted in the state of Awadh (also known as Oudh, in modern-day Uttar Pradesh), which had been annexed barely a year before.
In March 1858, Campbell once again advanced on Lucknow with a large army, meeting up with the force at Alambagh, this time seeking to suppress the rebellion in Awadh. He was aided by a large Nepalese contingent advancing from the north under Jang Bahadur. The forces drove the large but disorganised rebel army from Lucknow with the final fighting shooting on 21 March.~\cite{BOOK2}.

\begin{figure}[!h]
  \begin{center}
  \includegraphics[width=45mm,scale=0.5]{fight.jpg}
  \includegraphics[width=70mm,scale=0.65]{pesh.jpg}
  
  \caption{Battle in Kolapur(left) ; Execution of mutineers in Peshawar(right)}
  \label{Sorted Array}
  \end{center}
  \end{figure}


\vspace{1em}
A large-scale military uprising in the Punjab took place on 9 July, when most of a brigade of sepoys at Sialkot rebelled and began to move to Delhi. They were intercepted by John Nicholson with an equal British force as they tried to cross the Ravi River. After fighting steadily but unsuccessfully for several hours, the sepoys tried to fall back across the river but became trapped on an island. Three days later, Nicholson annihilated the 1,100 trapped sepoys in the Battle of Trimmu Ghat.

\vspace{1em}
Rani Lakshmi Bai\index{Rani Lakshmi Bai}, the Rani of Jhansi protested against the denial of rights of their adopted son. When war broke out, Jhansi quickly became a centre of the rebellion. On 1 June 1858 Rani Lakshmi Bai and a group of Maratha rebels captured the fortress city of Gwalior from the Scindia rulers, who were British allies.  The Rani died on 17 June, the second day of the Battle of Gwalior.~\cite{BOOK1}.



 

\section{Aftermath}

\begin{figure}[!h]
  \begin{center}
  \includegraphics[width=70mm,scale=0.65]{shah.jpg}
  %\includegraphics[width=70mm,scale=0.65]{luck.jpg}
  
  \caption{Surrender of Bahadur Shah Zafar}
  \label{Sorted Array}
  \end{center}
  \end{figure}

From the end of 1857, the British had begun to gain ground again. Lucknow was retaken in March 1858. On 8 July 1858, a peace treaty was signed and the rebellion ended. The last rebels were defeated in Gwalior on 20 June 1858. By 1859, rebel leaders Bakht Khan and Nana Sahib had either been slain or had fled.~\cite{BOOK2}.

 In all more than 100,000 Indians were killed in the Uprising and its aftermath.
\vspace{1em}



Bahadur Shah was tried for treason by a military commission assembled at Delhi, and exiled to Rangoon where he died in 1862~\cite{BOOK1}, bringing the Mughal dynasty to an end. In 1877 Queen Victoria \index{Queen Victoria}took the title of Empress of India~\cite{BOOK3}..
The rebellion saw the end of the East India Company's rule in India. In August, by the Government of India Act 1858, the company was formally dissolved and its ruling powers over India were transferred to the British Crown.~\cite{BOOK3}
\vspace{1em}

\begin{figure}[!h]
  \begin{center}
  \includegraphics[width=70mm,scale=0.65]{hang.jpg}
  %\includegraphics[width=70mm,scale=0.65]{luck.jpg}
  
  \caption{Punishment for mutiny}
  \label{Sorted Array}
  \end{center}
  \end{figure}

The Bengal army dominated the Indian army before 1857 and a direct result after the rebellion was the scaling back of the size of the Bengali contingent in the army.~\cite{BOOK2} The Brahmin presence in the Bengal Army was reduced because of their perceived primary role as mutineers. The British looked for increased recruitment in the Punjab for the Bengal army as a result of the apparent discontent that resulted in the Sepoy conflict.~\cite{BOOK4}
\vspace{1em}

The British increased the ratio of British to Indian soldiers within India. From 1861 Indian artillery was replaced by British units, except for a few mountain batteries.[140] The post-rebellion changes formed the basis of the military organisation of British India until the early 20th century.~\cite{BOOK1}




\bibliography{Master}
\printindex

\end{document}

