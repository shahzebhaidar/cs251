PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE STUDENTS(
 Roll  int PRIMARY KEY NOT NULL,
NAME varchar(100) NOT NULL,
Enrolled_in varchar(100),
Marks int);
.mode csv
.import student_input.csv STUDENTS



CREATE TABLE COURSES(
 ID  varchar(100) PRIMARY KEY NOT NULL,
NAME varchar(100) NOT NULL,
Student int ,
Midsem int,
Endsem int,

FOREIGN KEY(Student) REFERENCES STUDENTS(Roll));
.mode csv
.import course_input.csv COURSES

CREATE TABLE FACULTY(
ID int PRIMARY KEY NOT NULL,
NAME varchar(100) NOT NULL,
Department varchar(100),
Course varchar(100),

FOREIGN KEY(Course) REFERENCES COURSES(ID));
.mode csv
.import faculty_input.csv FACULTY

UPDATE STUDENTS SET Marks = (SELECT Midsem+Endsem FROM COURSES WHERE STUDENTS.Roll=COURSES.Student);


.headers on
.mode column
select * from STUDENTS;
select * from COURSES;
select * from FACULTY;


.mode csv
.headers on
.out student-details.csv
select * from STUDENTS;






COMMIT;
