
clear

cut -d, -f5 $1 > tmp

sed 's/.*,//' $1
#sed -i 's/[[:Dw:]]*$//' /$1

a=$(sed 's/a/a\n/g' tmp | grep -c "*")
b=$(sed 's/a/a\n/g' tmp | grep -c "F")
if((a>0))|| ((b>0)); then
	
	
	sed -i 's/*/Good/g' $1

	sed -i 's/,A/,Good/g' $1
	sed -i 's/Good./A/1' $1
	
	sed -i 's/,C/,Average/g' $1
	sed -i 's/Average./C/1' $1
	
	sed -i 's/,B/,Average/' $1
	sed -i 's/Average./B/1' $1
	
	sed -i 's/,D/,Poor/' $1
	sed -i 's/Poor./D/1' $1

	sed -i 's/,F/,Fail/' $1
	sed -i 's/Fail/.F/1' $1
    
    
fi	
cat $1
#sed -i 's/hello/bonjour/' $1

