#!/bin/sh
echo " Enter the files to be merged as \"file1 file2\"(without quotes) "
read filename   #stores name of both file1.csv file2.csv

paste -d, $filename > tmp  #merge the data of both files in csv format in tmp file

cut -d, -f1,2,4,6,7 tmp > output.txt #extract the required columns and paste in output.txt

rm tmp  #delete tmp file
clear

echo "output.txt created"