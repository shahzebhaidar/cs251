
echo "Enter the filename"
sum=0  # Stores the sum
a=0    # stores count
read filename    # read the filename
while read  line      # read line by line and store sum and increment a
do ((sum += line))
   ((a += 1))
done < $filename   # stop when file is empty

echo "scale=3;$sum/($a)"|bc -l  # print average correct to 3 decimal places
