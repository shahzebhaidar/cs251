#!/bin/sh
clear
 
awk  '
   {  tot += $i;              # tot stores the total sum
      if(NF!=0)  n+=1; }      # n stores the number of lines containing integers
   {
   	 
   }

   END{ 
   if(n!=0)  printf" The Average is %.2f\n",tot/n;          # if n is not zero print average
   else print  "No number found"; }' $1